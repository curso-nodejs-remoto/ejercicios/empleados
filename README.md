## Ejercicio - API Empleados

En este ejercicio le pedirás un listado de empleados a una API REST, y dependiendo de qué parámetro se le pase a la aplicación, se imprimirá una información u otra por consola.

1. A nuestro script se le llamará pasándole un parámetro llamado `accion` con uno de estos tres valores: `listar`, `total` o `salario-medio`
    `node app --accion=listar` -> lista todos los empleados
    `node app --accion=total` -> imprime el total de empleados
    `node app --accion=salario-medio` -> imprime el salario medio de los empleados

    Opcionalmente se le puede pasar un segundo parámetro llamado `delay` indicando un número de milisegundos, que usaremos en nuestra aplicación como delay. Si no se pasa, el delay será de 1 segundo.
    Consulta el anexo 1 para ver cómo recoger los parámetros

2. Instala el módulo node-fetch. Consulta el anexo 2 para ver cómo se hace una petición HTTP con él.

3. Crea una función llamada `cargaDatos` que reciba dos parámetros: un delay en milisegundos y una función callback
La función `cargaDatos` tiene que hacer una petición HTTP a http://dummy.restapiexample.com/api/v1/employees, que es una API REST que devuelve los datos en formato JSON, pero la petición la tiene que iniciar una vez haya transcurrido el número de milisegundos recibido.

4. Crea una función que reciba los datos de la API y liste todos los empleados, mostrando de cada uno su **nombre**, **edad** y **salario**. Si el parámetro pasado por línea de comandos es `listar`, pásale esta función a la función `cargaDatos`, con el delay correspondiente.

5. Crea una función que reciba los datos de la API e imprima la frase "Hay un total de X empleados" (siendo X el número total de empleados). Si el parámetro pasado por línea de comandos es `total`, pásale esta función a la función `cargaDatos`, con el delay correspondiente.

6. Crea una función que reciba los datos de la API e imprima la frase "El salario medio de los empleados es de X" (siendo X el salario medio de todos los empleados). Si el parámetro pasado por línea de comandos es `salario-medio`, pásale esta función a la función `cargaDatos`, con el delay correspondiente.

7. Bola extra: crea tres scripts en el archivo `package.json`, que abran la aplicación con cada uno de los parámetros del punto 1

8. Bola extra: reescribe la parte de la llamada a la API utilizando async / await.

### Anexo 1 - Recoger parámetros de la línea de comandos
Para recoger los parámetros de la línea de comandos vamos a usar el paquete `commander`:

```javascript
const { program } = require('commander');

program
    .option('-a --accion [accion]', 'Acción a realizar')
    .option('-d --delay [ms]', 'Milisegundos de delay', 1000)
    .parse(process.argv);
```
Los valores que se le han pasado a los parámetros los tendrás en `program.accion` y `program.delay`.

### Anexo 2 - Realizar una petición HTTP con el módulo node-fetch
1. fetch(URL) -> devuelve una promesa con la respuesta (el objeto Response, no los datos que nos da la API)
2. Esa respuesta tiene un método json() que devuelve una promesa con los datos que nos da la API
